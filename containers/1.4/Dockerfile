FROM hairyhenderson/gomplate:v3.11.7-slim@sha256:0a08ab8b9a4f06b8abbf7df8ba1acf3c36fec7be940f0281b48e0b14eaa6ff69 AS gomplate
FROM registry.gitlab.com/vito-containers/apkbuild-tools:1.0.27@sha256:38e3b8b86e5b63af8938b1899953190bae506b63b68aee757230935d0a43795c AS apkbuild-tools

FROM registry.gitlab.com/vito-containers/alpine:3.16.9@sha256:860978d22d1fd541247bc7d0eed2d33fd576c905cfffc4711714e2536999da8e AS builder
COPY --from=apkbuild-tools /usr/sbin/builder /usr/sbin/builder
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.16_apk_cache \
    builder prepare
COPY --chown=abuild:abuild builder/abuild /home/abuild
USER abuild
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.16_apk_cache \
    builder build

FROM registry.gitlab.com/vito-containers/alpine:3.16.9@sha256:860978d22d1fd541247bc7d0eed2d33fd576c905cfffc4711714e2536999da8e

ENV TZ=UTC

COPY --from=builder /home/abuild/.abuild/*.rsa.pub /etc/apk/keys/

# renovate: datasource=github-tags depName=mumble-voip/mumble
ARG MUMBLE_VERSION=1.4.287

# hadolint ignore=DL3019
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.16_apk_cache \
    --mount=type=cache,target=/packages,source=/home/abuild/packages/abuild,from=builder \
    set -eux; \
      echo '/packages' >> /etc/apk/repositories; \
      apk add \
        mumble="${MUMBLE_VERSION}-r99" \
      ; \
      sed '/^\/packages$/d' -i /etc/apk/repositories;

COPY --from=gomplate /gomplate /usr/sbin/gomplate
COPY rootfs /

ENV MUMBLE_DATABASE="/var/lib/mumble/database.sqlite" \
    MUMBLE_UNAME="mumble"

EXPOSE 64738/tcp 64738/udp
VOLUME /var/lib/mumble/

ENTRYPOINT [ "entrypoint" ]
CMD [ "mumble-server", "-ini", "/etc/mumble/config.ini" ]
